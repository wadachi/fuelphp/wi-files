<?php
/**
 * Wadachi FuelPHP Files Package
 *
 * Services for processing user files.
 *
 * @package    wi-files
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Fuel\Migrations;

class WiFiles_Create_Table_Files extends \Wi\Migration
{
  function up()
  {
    \DBUtil::create_table('files', [
      'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'comment' => 'ID'],
      'name' => ['type' => 'varchar', 'constraint' => 255, 'comment' => 'ファイル名'],
      'directory' => ['type' => 'varchar', 'constraint' => 255, 'comment' => 'ディレクトリ'],
      'hash' => ['type' => 'varchar', 'constraint' => 40, 'comment' => 'ハッシュ'],
      'mimetype' => ['type' => 'varchar', 'constraint' => 255, 'null' => true, 'comment' => 'MIMEタイプ'],
      'size' => ['type' => 'int', 'unsigned' => true, 'default' => 0, 'comment' => 'サイズ'],
      'downloads' => ['type' => 'int', 'unsigned' => true, 'default' => 0, 'comment' => 'ダウンロード数'],
      'created_at' => ['type' => 'timestamp', 'default' => \DB::expr('CURRENT_TIMESTAMP'), 'comment' => '作成日時'],
    ], ['id'], false, 'InnoDB', 'utf8_unicode_ci');  // !!! utf8_unicode_ci A = a = Ａ

    \DBUtil::create_index('files', ['directory', 'hash'], 'files_directory_hash', 'unique');

    $this->execute_sql([
      "GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE `files` TO '".\Config::get('db.default.connection.username')."'@'localhost'",
    ]);
  }

  function down()
  {
    $this->execute_sql([
      "GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE `files` TO '".\Config::get('db.default.connection.username')."'@'localhost'",
      "REVOKE SELECT, INSERT, UPDATE, DELETE ON TABLE `files` FROM '".\Config::get('db.default.connection.username')."'@'localhost'",
    ]);

    \DBUtil::drop_index('files', 'files_directory_hash');
    \DBUtil::drop_table('files');
  }
}
