<?php
/**
 * Wadachi FuelPHP Files Package
 *
 * Services for processing user files.
 *
 * @package    wi-files
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Fuel\Migrations;

class WiFiles_Files_Directory extends \Wi\Migration
{
  function up()
  {
    \Config::load('wi-files', true);

    $store_dir = \Config::get('wi-files.store_dir');
    @mkdir($store_dir, 0775, true);

    $store_group = \Config::get('wi-files.store_group');
    @chgrp($store_dir, $store_group);
  }

  function down()
  {
    \Config::load('wi-files', true);
    $store_dir = \Config::get('wi-files.store_dir');
    @rmdir($store_dir);
  }
}
