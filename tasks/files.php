<?php
/**
 * Wadachi FuelPHP Files Package
 *
 * Services for processing user files.
 *
 * @package    wi-files
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Fuel\Tasks;

\Package::load('wi-container');
\Package::load('wi-files');

use \Fuel;
use \Fuel\Core\Cli;
use \Fuel\Core\Config;
use \Wi\Container;

/**
 * ファイル タスク
 */
class Files
{
  // ファイル　サービス
  protected static $file_service = null;

  /**
   * 静的コンストラクタ
   *
   * @access public
   */
  public static function _init()// <editor-fold defaultstate="collapsed" desc="...">
  {
    Config::load('wi-files', true);
    static::$file_service = Container::get('service.file');
  }// </editor-fold>

  /**
   * ファイル処理を実行する
   * 実行の方法：php oil r Files
   *
   * @access public
   */
  public static function run()// <editor-fold defaultstate="collapsed" desc="...">
  {
    // 診断法
    Cli::write('FUEL_ENV: '.Fuel::$env);
    static::clean();

    return 0;
  }// </editor-fold>

  /**
   * 登録されていないファイルをゴミにする
   * 実行の方法：php oil r Files:clean
   *
   * @access public
   */
  public static function clean()// <editor-fold defaultstate="collapsed" desc="...">
  {
    $store_dir = Config::get('wi-files.store_dir');
    static::clean_directory($store_dir);
  }// </editor-fold>

  /**
   * 登録されていないファイルをゴミにする
   * 再帰的な関数
   *
   * @access private
   * @param string $store_dir ファイル保管場所の基本ディレクトリー
   * @param string sub_dir サブディレクトリー
   */
  private static function clean_directory($store_dir, $sub_dir = '')// <editor-fold defaultstate="collapsed" desc="...">
  {
    $trash_dir = Config::get('wi-files.trash_dir');
    $filenames = scandir($store_dir.DIRECTORY_SEPARATOR.$sub_dir);

    foreach($filenames as $filename)
    {
      // 現在ディレクトリーと親ディレクトリーをスキップする
      if ($filename === '.' || $filename === '..')
      {
        continue;
      }

      $path = $store_dir.DIRECTORY_SEPARATOR.$sub_dir.DIRECTORY_SEPARATOR.$filename;

      // サブディレクトリーに入る
      if(is_dir($path))
      {
          static::clean_directory($store_dir, $sub_dir.DIRECTORY_SEPARATOR.$filename);
          count(scandir($path)) == 2 and rmdir($path);
          continue;
      }

      // ファイルがデータベースに存在してない場合、ゴミにする
      $file = reset(static::$file_service->find_files([
        'directory' => $sub_dir,
        'hash' => $filename,
      ])['rows']);

      if (!$file)
      {
        @mkdir($trash_dir.DIRECTORY_SEPARATOR.$sub_dir, 0770, true);
        @rename($path, $trash_dir.DIRECTORY_SEPARATOR.$sub_dir.DIRECTORY_SEPARATOR.$filename);
      }
    }
  }// </editor-fold>
}
