<?php
/**
 * Wadachi FuelPHP Files Package
 *
 * Services for processing user files.
 *
 * @package    wi-files
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * ローカライズされた文字列
 * 言語：ja-JP
 */
return [
  'log' => [
    'missing_file' =>
      "データベースに記載されているファイルがディスク上に見つかりませんでした。\n".
      "ファイル名： :name\n",
      "ディレクトリ： :directory\n".
      "ハッシュ： :hash\n",
  ],
];
