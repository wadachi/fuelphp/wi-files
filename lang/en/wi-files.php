<?php
/**
 * Wadachi FuelPHP Files Package
 *
 * Services for processing user files.
 *
 * @package    wi-files
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * ローカライズされた文字列
 * 言語：ja-JP
 */
return [
  'log' => [
    'missing_file' =>
      "A file listed in the database could not be found on disk.\n".
      "Name: :name\n",
      "Directory: :directory\n".
      "Hash: :hash\n",
  ],
];
