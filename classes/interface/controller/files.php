<?php
/**
 * Wadachi FuelPHP Files Package
 *
 * Services for processing user files.
 *
 * @package    wi-files
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * ファイル　コントローラ　インターフェース
 */
interface Interface_Controller_Files
{
  // HTTPレスポンスコード
  const FILES_HTTP_FOUND = 200;
  const FILES_HTTP_NOT_FOUND = 404;

  /**
   * ファイルのヘッダを取得する
   *
   * @access public
   * @return \Fuel\Core\Response HTTPレスポンス
   */
  function head_index();

  /**
   * ファイルを取得する
   *
   * @access public
   * @return \Fuel\Core\Response HTTPレスポンス
   */
  function get_index();
}
