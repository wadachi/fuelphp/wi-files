<?php
/**
 * Wadachi FuelPHP Files Package
 *
 * Services for processing user files.
 *
 * @package    wi-files
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * ファイル サービス インタフェース
 */
interface Interface_Service_File
{
  /**
   * ファイルを削除する
   *
   * @access public
   * @param integer $id ファイルID
   */
  function delete_file($id);

  /**
   * ファイルを検索する
   *
   * @access public
   * @param array $criteria 検索条件
   *        string name ファイル名
   *        string directory ディレクトリ
   *        string hash ハッシュ
   *        string mimetype MIMEタイプ
   *        integer size サイズ
   *        integer size_min 最小サイズ
   *        integer size_max 最大サイズ
   *        integer downloads ダウンロード数
   *        integer downloads_min 最小ダウンロード数
   *        integer downloads_max 最大ダウンロード数
   *        string created_at 作成日時
   *        string created_at_from 作成日から
   *        string created_at_to まで作成日
   * @param array $options オプション
   *        boolean only_count カウントだけ行う
   *        string order_by ソート　フィールド名
   *        integer page ページ番号
   *        integer per_page ページごとの行数
   *        boolean reverse 反対ソート
   * @return array 検索結果
   */
  function find_files(array $criteria = [], array $options = []);

  /**
   * ファイルを取得する
   *
   * @access public
   * @param integer $id ファイルID
   * @return array ファイル
   */
  function get_file($id);

  /**
   * ダウンロード数を増分する
   *
   * @access public
   * @param integer $id ファイルID
   */
  function increment_download_count($id);

  /**
   * ファイルを保存する
   *
   * @access public
   * @param string $directory ディレクトリ
   * @param \Fuel\Upload\File $upload アップロードされたファイル
   * @return array ファイル
   */
  function save_file($directory, \Fuel\Upload\File $upload);

  /**
   * ログ項目の検索プロパティを検証する
   *
   * @access public
   * @param array $properties プロパティ
   *        string name ファイル名
   *        string directory ディレクトリ
   *        string hash ハッシュ
   *        string mimetype MIMEタイプ
   *        integer size サイズ
   *        integer size_min 最小サイズ
   *        integer size_max 最大サイズ
   *        integer downloads ダウンロード数
   *        integer downloads_min 最小ダウンロード数
   *        integer downloads_max 最大ダウンロード数
   *        string created_at 作成日時
   *        string created_at_from 作成日から
   *        string created_at_to まで作成日
   * @expectedexception \Wi\ValidationError 検証エラーがある場合
   */
  function validate_find_files(array $properties);
}
