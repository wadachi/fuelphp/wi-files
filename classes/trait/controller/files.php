<?php
/**
 * Wadachi FuelPHP Files Package
 *
 * Services for processing user files.
 *
 * @package    wi-files
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * コントローラのファイル形質
 */
trait Trait_Controller_Files
{
  // サービス
  protected $file_service;
  protected $log_service;

  /**
   * アクション前処理
   *
   * @access public
   */
  public function before()// <editor-fold defaultstate="collapsed" desc="...">
  {
    parent::before();
    $this->file_service = Container::get('service.file');
    $this->log_service = Container::get('service.log');
  }// </editor-fold>

  /**
   * ファイルのヘッダを取得する
   *
   * @access public
   * @return \Fuel\Core\Response HTTPレスポンス
   */
  public function head_index()// <editor-fold defaultstate="collapsed" desc="...">
  {
    $path = implode('/', func_get_args());

    $files = $this->file_service->find_files([
      'directory' => dirname($path),
      'hash' => basename($path)
    ]);

    $file = reset($files['rows']);

    if ( ! $file)
    {
      $this->response_status = self::FILES_HTTP_NOT_FOUND;
      return;
    }

    if ( ! file_exists($file['path']))
    {
      $this->log_missing_file($file);
      $this->response_status = self::FILES_HTTP_NOT_FOUND;
      return;
    }

    $headers = $this->forge_http_headers($file);

    if ($this->canXSendFile())
    {
      $headers['Accept-Ranges'] = 'bytes';
    }

    return \Response::forge(null, self::FILES_HTTP_FOUND, $headers);
  }// </editor-fold>

  /**
   * ファイルを取得する
   *
   * @access public
   * @return \Fuel\Core\Response HTTPレスポンス
   */
  public function get_index()// <editor-fold defaultstate="collapsed" desc="...">
  {
    $path = implode('/', func_get_args());

    $files = $this->file_service->find_files([
      'directory' => dirname($path),
      'hash' => basename($path)
    ]);

    $file = reset($files['rows']);

    if ( ! $file)
    {
      $this->response_status = self::FILES_HTTP_NOT_FOUND;
      return;
    }

    if ( ! file_exists($file['path']))
    {
      $this->log_missing_file($file);
      $this->response_status = self::FILES_HTTP_NOT_FOUND;
      return;
    }

    $this->file_service->increment_download_count($file['id']);

    if ($this->canXSendFile())
    {
      $headers = $this->forge_http_headers($file);
      $headers['Content-Disposition'] = 'attachment; filename="'.$file['name'].'"';
      $headers['X-Sendfile'] = $file['path'];

      Log::debug('X-Sendfile => '.$file['path']);
      return Response::forge(null, self::FILES_HTTP_FOUND, $headers);
    }

    \File::download($file['path'], $file['name']);
  }// </editor-fold>

  /**
   * Apacheモジュールmod_xsendfileがインストールされているかどうかを示す
   *
   * @access protected
   * @return モジュールmod_xsendfileがインストールされている場合はtrue；そうでない場合はfalse
   */
  protected function canXSendFile()// <editor-fold defaultstate="collapsed" desc="...">
  {
    return function_exists("apache_get_modules") && in_array('mod_xsendfile', apache_get_modules());
  }// </editor-fold>

  /**
   * ファイルのHTTPヘッダを作成する
   *
   * @access public
   * @param array $file ファイル
   * @return array ファイルのHTTPヘッダ
   */
  protected function forge_http_headers(array $file)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $locale = setlocale(LC_TIME, '0');
    try
    {
      setlocale(LC_TIME, 'en_US');
      $modified = $file['created_at']->format('%a, %d %b %Y %T %Z');
      setlocale(LC_TIME, $locale);
    } catch(Exception $error) {
      setlocale(LC_TIME, $locale);
      throw $error;
    }

    return [
      'Last-Modified' => $modified,
      'Content-Description' => 'File Transfer',
      'Content-Length' => $file['size'],
      'Content-Transfer-Encoding' => 'BINARY',
      'Content-Type' => $file['mimetype']
    ];
  }// </editor-fold>

  /**
   * ファイルがなかったをログに記録する
   *
   * @access public
   * @param array $msg_params メッセージ　パラメータ
   */
  protected function log_missing_file($msg_params)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $this->log_service->write('Application', [
      'level' => LogLevels::Info,
      'source' => 'wi-files',
      'event' => 'Download',
      'result' => 'Failure',
      'message' => \Lang::get('wi-files.log.missing_file', $msg_params),
    ]);
  }// </editor-fold>
}
