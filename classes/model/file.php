<?php
/**
 * Wadachi FuelPHP Files Package
 *
 * Services for processing user files.
 *
 * @package    wi-files
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * 監査ログ モデル
 */
class Model_File extends Model
{
  // テーブル名
  protected static $_table_name = 'files';

  // 保存プロパティ
  protected static $_properties = [
    'id' => [
      'data_type' => 'int',
    ],

    'name' => [
      'data_type' => 'varchar',
    ],

    'directory' => [
      'data_type' => 'varchar'
    ],

    'hash' => [
      'data_type' => 'varchar'
    ],

    'mimetype' => [
      'data_type' => 'varchar'
    ],

    'size' => [
      'data_type' => 'int'
    ],

    'downloads' => [
      'data_type' => 'int'
    ],

    'created_at' => [
      'data_type' => 'time_mysql'
    ],
  ];
}
