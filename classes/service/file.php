<?php
/**
 * Wadachi FuelPHP Files Package
 *
 * Services for processing user files.
 *
 * @package    wi-files
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * ファイル サービス
 */
class Service_File extends Service implements Interface_Service_File
{
  // コンテナでサービス名
  const _PROVIDES = 'service.file';

  // 検証
  private static $find_files_validator;

  /**
   * 静的コンストラクタ
   *
   * @access public
   */
  public static function _init()// <editor-fold defaultstate="collapsed" desc="...">
  {
    \Config::load('wi-files', true);
  }// </editor-fold>

  /**
   * ファイルを削除する
   *
   * @access public
   * @param integer $id ファイルID
   */
  public function delete_file($id)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $file = Model_File::find($id);
    $file->delete();
  }// </editor-fold>

  /**
   * ファイルを検索する
   *
   * @access public
   * @param array $criteria 検索条件
   *        integer id ID
   *        string name ファイル名
   *        string directory ディレクトリ
   *        string hash ハッシュ
   *        string mimetype MIMEタイプ
   *        integer size サイズ
   *        integer size_min 最小サイズ
   *        integer size_max 最大サイズ
   *        integer downloads ダウンロード数
   *        integer downloads_min 最小ダウンロード数
   *        integer downloads_max 最大ダウンロード数
   *        string created_at 作成日時
   *        string created_at_from 作成日から
   *        string created_at_to まで作成日
   * @param array $options オプション
   *        boolean only_count カウントだけ行う
   *        string order_by ソート　フィールド名
   *        integer page ページ番号
   *        integer per_page ページごとの行数
   *        boolean reverse 反対ソート
   * @return array 検索結果
   */
  public function find_files(array $criteria = [], array $options = [])// <editor-fold defaultstate="collapsed" desc="...">
  {
    $store_dir = \Config::get('wi-files.store_dir');
    $only_count = \Arr::get($options, 'only_count', false);
    $order_by = \Arr::get($options, 'order_by', 'created_at');
    $page = \Arr::get($options, 'page', 1) - 1;
    $per_page = \Arr::get($options, 'per_page', 0);
    $reverse = \Arr::get($options, 'reverse', true);

    $this->validate_find_files($criteria, $options);

    $query = Model_File::query();

    // 条件：最小サイズ
    if ( ! empty($criteria['size_min']))
    {
      $query->where('size', '>=', +$criteria['size_min']);
    }

    unset($criteria['size_min']);

    // 条件：最大サイズ
    if ( ! empty($criteria['size_max']))
    {
      $query->where('size', '<=', +$criteria['size_max']);
    }

    unset($criteria['size_max']);

    // 条件：最小ダウンロード数
    if ( ! empty($criteria['downloads_min']))
    {
      $query->where('downloads', '>=', +$criteria['downloads_min']);
    }

    unset($criteria['downloads_min']);

    // 条件：最大ダウンロード数
    if ( ! empty($criteria['downloads_max']))
    {
      $query->where('downloads', '<=', +$criteria['downloads_max']);
    }

    unset($criteria['downloads_max']);

    // 条件：作成日から
    if ( ! empty($criteria['created_at_from']))
    {
      $query->where('created_at', '>=', $criteria['created_at_from']);
    }

    unset($criteria['created_at_from']);

    // 条件：まで作成日
    if ( ! empty($criteria['created_at_to']))
    {
      // 1日追加（包括日付）
      $date_to = \Date::create_from_string($criteria['created_at_to']);
      $date_to = new \DateTime('@'.$date_to->get_timestamp());
      $date_to->add(new \DateInterval('P1D'));
      $date_to = \Date::forge($date_to->getTimestamp());
      $date_to = $date_to->format('ja');
      $query->where('created_at', '<', $date_to);
    }

    unset($criteria['created_at_to']);

    // その他の条件
    foreach ($criteria as $key => $value)
    {
      if ( ! empty($value))
      {
        $query->where($key, $value);
      }
    }

    // 集計
    $count = $query->count();

    // 取得
    if ( ! $only_count)
    {
      $query->order_by($order_by, $reverse ? 'DESC' : 'ASC');
      $per_page > 0 and $query->rows_offset($page * $per_page);
      $per_page > 0 and $query->rows_limit($per_page);
      $rows = $query->get();

      // 配列に変換
      foreach ($rows as $key => $row) {
        $rows[$key] = $row->to_array();
        $rows[$key]['path'] = $store_dir.'/'.$rows[$key]['directory'].'/'.$rows[$key]['hash'];
      }
    }
    else
    {
      $rows = [];
    }

    return [
      'count' => $count,
      'page' => $page + 1,
      'per_page' => $per_page,
      'rows' => $rows,
      'order_by' => $order_by,
      'reverse' => $reverse,
    ];
  }// </editor-fold>

  /**
   * ファイルを取得する
   *
   * @access public
   * @param integer $id ファイルID
   * @return array ファイル
   */
  public function get_file($id)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $store_dir = \Config::get('wi-files.store_dir');
    $file = Model_File::find($id);

    if ($file)
    {
      $file = $file->to_array();
      $file['path'] = $store_dir.'/'.$file['directory'].'/'.$hash;
      return $file;
    }

    return false;
  }// </editor-fold>

  /**
   * ダウンロード数を増分する
   *
   * @access public
   * @param integer $id ファイルID
   */
  public function increment_download_count($id)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $file = Model_File::find($id);

    if ($file)
    {
      $file->downloads = $file->downloads + 1;
      $file->save();
    }
  }// </editor-fold>

  /**
   * ファイルを保存する
   *
   * @access public
   * @param string $directory ディレクトリ
   * @param \Fuel\Upload\File $upload アップロードされたファイル
   * @return array ファイル
   */
  public function save_file($directory, \Fuel\Upload\File $upload)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $hash = sha1_file($upload->tmp_name);
    $store_dir = \Config::get('wi-files.store_dir');

    $files = $this->find_files([
      'directory' => $directory,
      'hash' => $hash,
    ]);

    $file = reset($files['rows']);

    if ($file)
    {
      $file['path'] = $store_dir.'/'.$directory.'/'.$hash;
      return $file;
    }

    if ($upload->isValid())
    {
      $file = Model_File::forge([
        'name' => $upload->name,
        'directory' => $directory,
        'hash' => $hash,
        'mimetype' => $upload->mimetype,
        'size' => $upload->size,
        'downloads' => 0,
      ]);

      $file->save();

      $upload->setConfig([
        'path' => $store_dir.'/'.$directory,
        'new_name' => $hash
      ]);

      $upload->filename = $hash;
      $upload->extension = '';
      $upload->save();

      $file = $file->to_array();
      $file['path'] = $store_dir.'/'.$directory.'/'.$hash;
      return $file;
    }

    return false;
  }// </editor-fold>

  /**
   * ログ項目の検索プロパティを検証する
   *
   * @access public
   * @param array $properties プロパティ
   *        integer id ID
   *        string name ファイル名
   *        string directory ディレクトリ
   *        string hash ハッシュ
   *        string mimetype MIMEタイプ
   *        integer size サイズ
   *        integer size_min 最小サイズ
   *        integer size_max 最大サイズ
   *        integer downloads ダウンロード数
   *        integer downloads_min 最小ダウンロード数
   *        integer downloads_max 最大ダウンロード数
   *        string created_at 作成日時
   *        string created_at_from 作成日から
   *        string created_at_to まで作成日
   * @expectedexception \Wi\ValidationError 検証エラーがある場合
   */
  public function validate_find_files(array $properties)// <editor-fold defaultstate="collapsed" desc="...">
  {
    if (!isset(static::$find_files_validator))
    {
      $v = static::$find_files_validator = \Validation::forge(get_class().'_Find_Files_Validator');
      $v->add_field('id', 'ID', 'trim|valid_string[numeric]|numeric_max[4294967295]');
      $v->add_field('name', 'ファイル名', 'trim|max_length[255]|valid_string[alpha,utf8,specials,numeric,spaces,punctuation,dashes,quotes,brackets,braces]'); // -slashes
      $v->add_field('directory', 'ディレクトリ', 'trim|max_length[255]');
      $v->add_field('hash', 'ハッシュ', 'trim|max_length[40]|valid_string[alpha_numeric]');
      $v->add_field('mimetype', 'MIMEタイプ', 'trim|max_length[255]');
      $v->add_field('size', 'サイズ', 'trim|valid_string[numeric]|numeric_max[4294967295]');
      $v->add_field('size_min', '最小サイズ', 'trim|valid_string[numeric]|numeric_max[4294967295]');
      $v->add_field('size_max', '最大サイズ', 'trim|valid_string[numeric]|numeric_max[4294967295]');
      $v->add_field('downloads', 'ダウンロード数', 'trim|valid_string[numeric]|numeric_max[4294967295]');
      $v->add_field('downloads_min', '最小ダウンロード数', 'trim|valid_string[numeric]|numeric_max[4294967295]');
      $v->add_field('downloads_max', '最大ダウンロード数', 'trim|valid_string[numeric]|numeric_max[4294967295]');
      $v->add_field('created_at', '作成日時', 'trim|valid_date[Y/m/d H:i:s]');
      $v->add_field('created_at_from', '作成日から', 'trim|valid_date[Y/m/d H:i:s]');
      $v->add_field('created_at_to', 'まで作成日', 'trim|valid_date[Y/m/d H:i:s]');
    }

    if (!static::$find_files_validator->run($properties))
    {
      throw new ValidationError(static::$find_files_validator->error_message());
    }
  }// </editor-fold>
}
