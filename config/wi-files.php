<?php
/**
 * Wadachi FuelPHP Files Package
 *
 * Services for processing user files.
 *
 * @package    wi-files
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * ファイル設定
 */
return [
  /*
   * ファイル保管場所の基本ディレクトリー
   */
  'store_dir' => APPPATH.'../../files',

  /*
   * ファイル保管場所の許可グループ
   */
  'store_group' => 'apache',

  /*
   * ゴミファイルの基本ディレクトリー
   */
  'trash_dir' => APPPATH.'../../trash',
];
