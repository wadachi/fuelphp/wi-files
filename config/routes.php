<?php
/**
 * Wadachi FuelPHP Files Package
 *
 * Services for processing user files.
 *
 * @package    wi-files
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

return [
  'files/(:any)' => [
    ['HEAD', new Route('files/index/$1')],
    ['GET', new Route('files/index/$1')],
  ],
];
