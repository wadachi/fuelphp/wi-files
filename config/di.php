<?php
/**
 * Wadachi FuelPHP Files Package
 *
 * Services for processing user files.
 *
 * @package    wi-files
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * 依存性注入コンテナの設定
 */
return [
  'service.file' => [
    'class' => 'Wi\\Service_File',
  ],
];
