<?php
/**
 * Wadachi FuelPHP Files Package
 *
 * Services for processing user files.
 *
 * @package    wi-files
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

\Package::load([
  'wi-container',
  'wi-migration',
  'wi-model',
  'wi-validation',
]);

\Autoloader::add_classes([
  'Wi\\Interface_Controller_Files' => __DIR__.'/classes/interface/controller/files.php',
  'Wi\\Interface_Service_File' => __DIR__.'/classes/interface/service/file.php',
  'Wi\\Model_File' => __DIR__.'/classes/model/file.php',
  'Wi\\Service_File' => __DIR__.'/classes/service/file.php',
  'Wi\\Trait_Controller_Files' => __DIR__.'/classes/trait/controller/files.php',
]);
